﻿using moviesSam.Services;
using Ninject.Modules;


namespace moviesSam.IoC
{
    class IocNinject : NinjectModule
    {
        public override void Load()
        {
            Bind<ITMDbService>().To<TMDDbService>();
        }
    }
}
