﻿using Ninject;
using Microsoft.Practices.ServiceLocation;

namespace moviesSam.IoC
{
    class Bootstrapper
    {
        private static IKernel Container { get; set; }
        public static void Initialize()
        {
            Container = new StandardKernel(new IocNinject());
            ServiceLocator.SetLocatorProvider(() => new NinjectServiceLocator(Container));
           
        }
        public static void ShutDown()
        {
            Container.Dispose();
        }
    }
}
