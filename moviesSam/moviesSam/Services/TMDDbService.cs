﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Net.TMDb;
using moviesSam.ClientSingleton;

namespace moviesSam.Services
{
    public class TMDDbService : ITMDbService
    {
        private string language = "English";

        /// <summary>
        /// The method returns a collection of the top movies 
        /// </summary>
        /// <param name="cancellationToken">Сancellation request token</param>
        /// <param name="numberPage">The page number to display the result</param>
        /// <returns>Task<ObservableCollection<Movie>></returns>
        public async Task<ObservableCollection<Movie>> GetTopRatedMovies(CancellationToken cancellationToken, int numberPage, int countElementCollection = 0)
        {
            ObservableCollection<Movie> topMoviesCollection;
            ServiceClient client = ServiceClientHandler.GetInstance();
            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();               
                PagedResult<Movie> movies = await client.Movies.GetTopRatedAsync(null, numberPage, cancellationToken);
                AddElementsToObservableCollection(countElementCollection, movies, out topMoviesCollection);
                return topMoviesCollection;
            }           
        }


        /// <summary>
        /// The method returns information about the movie for specific movie id
        /// </summary>
        /// <param name="cancellationToken">Сancellation request token</param>
        /// <param name="idMovie">Id movie to search for information</param>
        /// <returns>Task<Movie></Movie></returns>
        public async Task<Movie> GetMovieInformationById(CancellationToken cancellationToken, int idMovie)
        {
            Movie movieInformation = new Movie();
            ServiceClient client = ServiceClientHandler.GetInstance();
            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();
                movieInformation = await client.Movies.GetAsync(idMovie, language, true, cancellationToken);
                return movieInformation;
            }
        }

        /// <summary>
        /// Rerurns a list of similar movies
        /// </summary>
        /// <param name="cancellationToken">Сancellation request token</param>
        /// <param name="idMovie">Id movie to search for similar movies</param>
        /// <param name="countElementCollection">Specifies how to search for movies</param>
        /// <returns>Task<ObservableCollection<Movie>></returns>
        public async Task<ObservableCollection<Movie>> GetSimilarMovie(CancellationToken cancellationToken, int idMovie, int countElementCollection = 0)
        {
            ObservableCollection<Movie> similarpMoviesCollection;
            ServiceClient client = ServiceClientHandler.GetInstance();
            PagedResult<Movie> movies = await client.Movies.GetSimilarAsync(idMovie, language, 1, cancellationToken);
            AddElementsToObservableCollection(countElementCollection, movies, out similarpMoviesCollection);
            return similarpMoviesCollection;
        }

        /// <summary>
        /// Returns a list of actors
        /// </summary>
        /// <param name="cancellationToken">Сancellation request token</param>
        /// <param name="idMovie">Id movie to search for actors</param>
        /// <param name="countActors">Specifies how to search for actors</param>
        /// <returns>Task<ObservableCollection<MediaCast>></returns>
        public async Task<ObservableCollection<MediaCast>> GetActorsForMovie(CancellationToken cancellationToken, int idMovie, int countActors)
        {
            ObservableCollection<MediaCast> actorsForMovieCollection;
            ServiceClient client = ServiceClientHandler.GetInstance();
            IEnumerable<MediaCredit> credit = await client.Movies.GetCreditsAsync(idMovie, cancellationToken);
            AddElementsToObservableCollection(countActors, credit, out actorsForMovieCollection);
            return actorsForMovieCollection;
        }


        public async Task<ObservableCollection<Show>> GetPopularShows(CancellationToken cancellationToken, int numberPage)
        {
            ObservableCollection<Show> popularShowsCollection;
            ServiceClient client = ServiceClientHandler.GetInstance();
            PagedResult<Show> shows = await client.Shows.GetPopularAsync(language, numberPage, cancellationToken);
            List<Show> showList = shows.Results.ToList<Show>();
            popularShowsCollection = new ObservableCollection<Show>(showList);          
            return popularShowsCollection;
        }

        public async Task <ObservableCollection<Resource>> SearchAll(CancellationToken cancellationToken, string querySearch, int numberPage)
        {
            ObservableCollection<Resource> resResult = new ObservableCollection<Resource>();
            Resources resources = new Resources();
            if (querySearch != string.Empty)
            {
                ServiceClient client = ServiceClientHandler.GetInstance();
                Movies movies = await client.Movies.SearchAsync(querySearch, language, true, null, numberPage, cancellationToken);
                Shows shows = await client.Shows.SearchAsync(querySearch, language, null, numberPage, cancellationToken);
                People people = await client.People.SearchAsync(querySearch, true, numberPage, cancellationToken);
                foreach (Movie m in movies.Results)
                {
                    resResult.Add((Resource)m);
                }

                foreach (Show sh in shows.Results)
                {
                    resResult.Add((Resource)sh);
                }

                foreach (Person p in people.Results)
                {
                    resResult.Add((Resource)p);
                }              
            }
            return resResult;         
        }


        private void AddElementsToObservableCollection(int countElement, PagedResult<Movie> movies, out ObservableCollection<Movie> observableCollection)
        {
            observableCollection = new ObservableCollection<Movie>();
            if (countElement == 0)
                countElement = movies.Results.Count();
            int i = 0;
            foreach (Movie movie in movies.Results)
            {
                i++;
                observableCollection.Add(movie);
                if (i == countElement)
                    return;
            }
        }

        private void AddElementsToObservableCollection(int countElement, IEnumerable<MediaCredit> mediaCredit, out ObservableCollection<MediaCast> actorsForMovieCollection)
        {
            actorsForMovieCollection = new ObservableCollection<MediaCast>();
            int i = 0;
            foreach (MediaCast mediaCast in mediaCredit)
            {
                i++;
                actorsForMovieCollection.Add(mediaCast);
                if (i == countElement)
                    return;
            }
        }
    }
}
 