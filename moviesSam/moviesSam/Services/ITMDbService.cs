﻿using System.Threading.Tasks;
using System.Threading;
using System.Collections.ObjectModel;
using System.Net.TMDb;

namespace moviesSam.Services
{
    /// <summary>
    /// Provodes methods for interacting with ServiceClient (Net.TMDb.dll)
    /// </summary>
    interface ITMDbService
    {
        #region For Movie
        Task<ObservableCollection<Movie>> GetTopRatedMovies(CancellationToken cancellationToken, int numberPage, int countElementCollection=0);

        Task <Movie> GetMovieInformationById(CancellationToken cancellationToken, int idMovie);

        Task <ObservableCollection<MediaCast>> GetActorsForMovie(CancellationToken cancellationToken, int idMovie, int countActors);

        Task <ObservableCollection<Movie>> GetSimilarMovie(CancellationToken cancellationToken, int idMovie, int countElementCollection=0);
        #endregion

        #region For Show
        Task<ObservableCollection<Show>> GetPopularShows(CancellationToken cancellcancellationToken, int numberPage);
        #endregion

        Task <ObservableCollection<Resource>> SearchAll (CancellationToken cancellcancellationToken, string querySearch,int numberPage);
    }
}
