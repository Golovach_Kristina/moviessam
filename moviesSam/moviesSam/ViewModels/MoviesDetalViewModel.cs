﻿using System.Threading;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Linq;
using System.Collections.Generic;
using System.Net.TMDb;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using moviesSam.Services;
using moviesSam.Navigation;

namespace moviesSam.ViewModels
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// </summary>
    class MoviesDetalViewModel : ViewModelBase
    {
        private const string URL_IMAGE = @"http://image.tmdb.org/t/p/original";
        ITMDbService _service;

        private string _urlImage;
        private Movie _selectedMovie;
        private ObservableCollection<Movie> _similarMovies;
        private ObservableCollection<MediaCast> _actorsForMovie;
        private ObservableCollection<string> _genres;
        

        public ICommand ShowSelectedSimilarMovieCommand { get; private set; }

        public Movie SelectedMovie
        {
            get   { return _selectedMovie; }
            set
            {
                _selectedMovie = value;
                RaisePropertyChanged();              
            }
        }

       
        public ObservableCollection<Movie> SimilarMovies
        {
            get { return _similarMovies; }
            set
            {
                _similarMovies = value;
                RaisePropertyChanged();
            }
        }
        
        public ObservableCollection<MediaCast> ActorsForMovie
        {
            get { return _actorsForMovie; }
            set
            {
                _actorsForMovie = value;
                RaisePropertyChanged();
            }
        }
        
        public ObservableCollection<string> Genres
        {
            get { return _genres; }
            set
            {
                _genres = value;
                RaisePropertyChanged();
            }
        }

        
        public string UrlImage
        {
            get { return URL_IMAGE + SelectedMovie.Backdrop; }
            set
            {
                _urlImage = value;
                RaisePropertyChanged();
            }

        }

        /// <summary>
        /// Initializes a new instance of the MoviesDetalViewModel class.
        /// </summary>
        public MoviesDetalViewModel(ITMDbService service, Movie selectedMovie)
        {
            _service = service;
            SelectedMovie = selectedMovie;
            GetInfo();
            GetActors(5);
            GetSimilarMovie(7);
            ShowSelectedSimilarMovieCommand = new RelayCommand<Movie>(ShowSelectedSimilarMovie);
        }

        private void ShowSelectedSimilarMovie(Movie movie)
        {
            SelectedMovie = movie;
            UrlImage = URL_IMAGE + SelectedMovie.Backdrop;
            GetInfo();
            GetActors(5);
            GetSimilarMovie(7);
            StackNavigation.ViwModelInStack.Push(this);
        }

        private async void GetInfo()
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cansellationToken = cancellationTokenSource.Token;
            SelectedMovie = await _service.GetMovieInformationById(cansellationToken, SelectedMovie.Id);
            IEnumerable<string> genres = SelectedMovie.Genres.Select(genre => genre.Name);
            Genres = new ObservableCollection<string>(genres.Where(x => x.IsNormalized()));
        }

        private async void GetActors(int countActors)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cansellationToken = cancellationTokenSource.Token;
            ActorsForMovie = await _service.GetActorsForMovie(cansellationToken, SelectedMovie.Id, countActors);
        }

        private async void GetSimilarMovie(int countSimilarMovie = 0)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cansellationToken = cancellationTokenSource.Token;
            SimilarMovies = await _service.GetSimilarMovie(cansellationToken, SelectedMovie.Id, countSimilarMovie);
        }
    }
}