﻿using System.Collections.ObjectModel;
using System.Net.TMDb;
using GalaSoft.MvvmLight;
using moviesSam.Services;


namespace moviesSam.ViewModels
{
    class ActorsViewModel : ViewModelBase
    {
        private ITMDbService _service;

        private ObservableCollection<Person> _personCollection;
        public ObservableCollection<Person> PersonCollection
        {
            get { return _personCollection; }
            set
            {
                _personCollection = value;
                RaisePropertyChanged();
            }
        }

        public ActorsViewModel(ITMDbService service)
        {
            _service = service;
        }
    }
}
