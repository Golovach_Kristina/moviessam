﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.TMDb;
using GalaSoft.MvvmLight;
using moviesSam.Services;
using moviesSam.Navigation;
using System.Collections.ObjectModel;
using System.Threading;

namespace moviesSam.ViewModels
{
   class TVShowsViewModel: ViewModelBase
    {
        private ITMDbService _service;
        private ObservableCollection<Show> _showCollection;

        public ObservableCollection<Show> ShowCollection
        {
            get
            {
                if (_showCollection == null)
                {
                    GetPopular(1);
                    return _showCollection;
                }
                return _showCollection;
            }
            set
            {
                _showCollection = value;
                RaisePropertyChanged();
            }
        }

        public TVShowsViewModel(ITMDbService service)
        {
            _service = service;
        }

        private async void GetPopular(int numberPage)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;
            ShowCollection = await _service.GetPopularShows(cancellationToken, numberPage);
        }
    }
}
