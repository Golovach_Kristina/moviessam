﻿using System;
using System.Net.TMDb;
using System.Collections.ObjectModel;
using System.Threading;
using moviesSam.Services;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;

namespace moviesSam.ViewModels
{
    delegate void ShowSelectedMovieHandler();

    class MoviesViewModel : ViewModelBase
    {
        ITMDbService _service;

        private ObservableCollection<Movie> _movieCollection;
        private Movie _selectedMovie;
        public event ShowSelectedMovieHandler ShowSelectedMovieEvent = () => { };

        public ObservableCollection<Movie> MovieCollection
        {
            get
            {
                if (_movieCollection == null)
                {
                    GetTopRated(1);
                    return _movieCollection;
                }
                return _movieCollection;
            }
            set
            {
                _movieCollection = value;
                RaisePropertyChanged("MovieCollection");
            }
        }

        public Movie SelectedMovie
        {
            get { return _selectedMovie; }
            set
            {
                _selectedMovie = value;
                RaisePropertyChanged();
            }
        }

        public ICommand ShowSelectedMovieCommand { get; set; }

        public MoviesViewModel(ITMDbService service)
        {
            _service = service;
            ShowSelectedMovieCommand = new RelayCommand(() => ShowSelectedMovie());
        }                   

        private void ShowSelectedMovie()
        {
            ShowSelectedMovieEvent();
        }

        private async void GetTopRated(int numberPage)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;
            try
            {
                MovieCollection = await _service.GetTopRatedMovies(cancellationToken, numberPage);
            }
            catch (Exception ex)
            {
                //  MessengerInstance = ex.Message;
            }
            finally
            {
                cancellationTokenSource.Dispose();
            }

        }

    }
}
