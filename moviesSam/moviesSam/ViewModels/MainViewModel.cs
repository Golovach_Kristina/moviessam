﻿using System.Linq;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Threading;
using System.Net.TMDb;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using moviesSam.Services;
using moviesSam.Navigation;

namespace moviesSam.ViewModels
{
   public class MainViewModel : ViewModelBase
    {
        private ViewModelBase _currentViewModel;

        readonly static MoviesViewModel _moviesViewModel =  new MoviesViewModel(new TMDDbService());
        readonly static TVShowsViewModel _tvShowsViewModel = new TVShowsViewModel(new TMDDbService());
        readonly static ActorsViewModel _actorsViewModel = new ActorsViewModel(new TMDDbService());

        public ICommand MoviesViewCommand { get; set; }
        public ICommand TVShowsViewCommand { get; set; }
        public ICommand ActorsViewCommand { get; set; }
        public ICommand SearchAllCommand { get; set; }
        public ICommand BackCommand { get; set; }

        public ViewModelBase CurrentViewModel
        {
            get { return _currentViewModel; }
            set
            {
                _currentViewModel = value;
                RaisePropertyChanged();
            }
        }

        public MainViewModel()
        {
            CurrentViewModel = _moviesViewModel;
            MoviesViewCommand = new RelayCommand(() => ExecuteMoviesViewCommand());
            TVShowsViewCommand = new RelayCommand(() => ExecuteTVShowsViewCommand());
            ActorsViewCommand = new RelayCommand(() => ExecuteActorsViewCommand());
            SearchAllCommand = new RelayCommand<string>(ExecuteSearchAllCommand);
            BackCommand = new RelayCommand(()=>ExecuteBackCommand());

            _moviesViewModel.ShowSelectedMovieEvent += _moviesViewModel_ShowSelectedMovieEvent;
        }

        private void ExecuteBackCommand()
        {
            if (StackNavigation.ViwModelInStack.Count != 0)
            {
                CurrentViewModel = StackNavigation.ViwModelInStack.Pop();
            }                     
        }

        private void _moviesViewModel_ShowSelectedMovieEvent()
        {
            CurrentViewModel = new MoviesDetalViewModel(new TMDDbService(), _moviesViewModel.SelectedMovie);
        }

        private void  ExecuteMoviesViewCommand()
        {            
            CurrentViewModel = _moviesViewModel;                                   
        }

        private void ExecuteTVShowsViewCommand()
        {
            CurrentViewModel = _tvShowsViewModel;
        }

        private void ExecuteActorsViewCommand()
        {
            CurrentViewModel = _actorsViewModel;
        } 
        
        private  async void ExecuteSearchAllCommand(string query)
        {
            TMDDbService service = new TMDDbService();
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;
            ObservableCollection<Resource> resourses = await service.SearchAll(cancellationToken, query, 1);
            if (resourses.Count != 0)
            {
                _moviesViewModel.MovieCollection = new ObservableCollection<Movie>(resourses.Where(r => r.GetType().Name == "Movie").Cast<Movie>());
                _tvShowsViewModel.ShowCollection = new ObservableCollection<Show>(resourses.Where(r => r.GetType().Name == "Show").Cast<Show>());
                _actorsViewModel.PersonCollection = new ObservableCollection<Person>(resourses.Where(r => r.GetType().Name == "Person").Cast<Person>());               
            }
            else
            {
                if (CurrentViewModel is MoviesDetalViewModel)
                {
                    CurrentViewModel = new MoviesViewModel(new TMDDbService());
                }
                if (CurrentViewModel is TVShowsViewModel)
                {
                    CurrentViewModel = new TVShowsViewModel(new TMDDbService());
                }
            }
        }
    }
}
