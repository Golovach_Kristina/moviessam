﻿using System.Net.TMDb;

namespace moviesSam.ClientSingleton
{
    /// <summary>
    /// The class provides singleton instance of the ServiceClient (Net.TMDb.dll)
    /// </summary>
    class ServiceClientHandler
    {
        public const string API_KEY = "528cf5efd34f6a3d720f8abfe44a554c";
        private static  ServiceClient clientHandler;

        #region constructor
        private ServiceClientHandler()
        { }

        public static ServiceClient GetInstance()
        {
            if (clientHandler == null)
            {
                clientHandler = new ServiceClient(API_KEY);
            }
            return clientHandler;
        }
        #endregion

        public void RemoveInstance()
        {
            clientHandler.Dispose();
        }
    }
}
