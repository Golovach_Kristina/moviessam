﻿using System.Collections.Generic;
using GalaSoft.MvvmLight;


namespace moviesSam.Navigation
{
    /// <summary>
    /// The class storage for navigation ViewModels
    /// </summary>
    public static class StackNavigation
    {
        public static Stack<ViewModelBase> ViwModelInStack = new Stack<ViewModelBase>();
    }
}
